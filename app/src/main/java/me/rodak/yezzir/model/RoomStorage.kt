package me.rodak.yezzir.model

import me.rodak.yezzir.model.room.Database
import me.rodak.yezzir.model.room.entity.GroupEntity
import me.rodak.yezzir.model.room.entity.TaskEntity

@Suppress("unused")
class RoomStorage(database: Database) {
    private val tasksDao = database.tasksDao()
    private val groupsDao = database.groupsDao()
    val tasks by lazy {
        tasksDao.getAll()
    }
    val groups by lazy {
        groupsDao.getAll()
    }

    suspend fun insertTask(task: TaskEntity) = tasksDao.insert(task)
    suspend fun updateTask(task: TaskEntity) = tasksDao.update(task)
    suspend fun deleteTask(task: TaskEntity) = tasksDao.delete(task)
    suspend fun deleteAllTasks() = tasksDao.deleteAll()

    suspend fun insertGroup(group: GroupEntity) = groupsDao.insert(group)
    suspend fun updateGroup(group: GroupEntity) = groupsDao.update(group)
    suspend fun deleteGroup(group: GroupEntity) = groupsDao.delete(group)
    suspend fun deleteAllGroups() = groupsDao.deleteAll()
}