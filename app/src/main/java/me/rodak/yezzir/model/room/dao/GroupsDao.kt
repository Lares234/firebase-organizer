package me.rodak.yezzir.model.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import me.rodak.yezzir.model.room.Database.Companion.GROUPS_TABLE
import me.rodak.yezzir.model.room.entity.GroupEntity

@Dao
interface GroupsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(group: GroupEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(group:GroupEntity)

    @Delete
    suspend fun delete(group: GroupEntity)

    @Query("DELETE from $GROUPS_TABLE")
    suspend fun deleteAll()

    @Query("SELECT * FROM $GROUPS_TABLE ORDER BY id ASC")
    fun getAll(): LiveData<List<GroupEntity>>
}