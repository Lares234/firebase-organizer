package me.rodak.yezzir.model.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import me.rodak.yezzir.model.room.Database.Companion.TASKS_TABLE
import me.rodak.yezzir.model.room.entity.TaskEntity

@Dao
interface TasksDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(task: TaskEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(task: TaskEntity)

    @Delete
    suspend fun delete(task: TaskEntity)

    @Query("DELETE from $TASKS_TABLE")
    suspend fun deleteAll()

    @Query("SELECT * FROM $TASKS_TABLE ORDER BY id ASC")
    fun getAll(): LiveData<List<TaskEntity>>
}