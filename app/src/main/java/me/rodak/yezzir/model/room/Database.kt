package me.rodak.yezzir.model.room

import androidx.room.Database
import androidx.room.RoomDatabase
import me.rodak.yezzir.model.room.Database.Companion.SCHEMA_VERSION
import me.rodak.yezzir.model.room.dao.GroupsDao
import me.rodak.yezzir.model.room.dao.TasksDao
import me.rodak.yezzir.model.room.entity.GroupEntity
import me.rodak.yezzir.model.room.entity.TaskEntity

@Database(
    entities = [TaskEntity::class, GroupEntity::class],
    version = SCHEMA_VERSION
)
abstract class Database : RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "user-database"
        const val SCHEMA_VERSION = 1
        const val GROUPS_TABLE = "groups"
        const val TASKS_TABLE = "tasks"
    }

    abstract fun tasksDao(): TasksDao
    abstract fun groupsDao(): GroupsDao
}