package me.rodak.yezzir.model.room.entity

import androidx.room.*
import me.rodak.yezzir.model.room.Database.Companion.TASKS_TABLE

@Entity(
    tableName = TASKS_TABLE,
    foreignKeys = [ForeignKey(
        entity = GroupEntity::class,
        parentColumns = ["id"],
        childColumns = ["group_id"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class TaskEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = -1,
    @ColumnInfo(name = "group_id") val groupId: Long,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "notes") val notes: String,
    @ColumnInfo(name = "is_important") val isImportant: Boolean,
    @ColumnInfo(name = "is_completed") val isCompleted: Boolean = false
)