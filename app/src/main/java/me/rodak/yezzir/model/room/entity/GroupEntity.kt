package me.rodak.yezzir.model.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import me.rodak.yezzir.model.room.Database.Companion.GROUPS_TABLE

@Entity(tableName = GROUPS_TABLE)
data class GroupEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = -1,
    @ColumnInfo(name = "name") val name: String
)