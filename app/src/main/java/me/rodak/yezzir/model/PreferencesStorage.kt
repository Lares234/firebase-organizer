package me.rodak.yezzir.model

import android.content.SharedPreferences

class PreferencesStorage(private val preferences: SharedPreferences) {
    companion object {
        const val KEY_USER_ACCOUNT_CREATED = "user_account_created"
    }

    var userAccountCreated: Boolean
        get() = preferences.getBoolean(KEY_USER_ACCOUNT_CREATED, false)
        set(value) {
            preferences.edit().putBoolean(KEY_USER_ACCOUNT_CREATED, value).apply()
        }

}