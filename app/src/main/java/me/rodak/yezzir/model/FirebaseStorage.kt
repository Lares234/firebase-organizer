package me.rodak.yezzir.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.RuntimeException

class FirebaseStorage(private val firebaseAuth: FirebaseAuth, private val firestore: FirebaseFirestore) {
    private val mutableCurrentUser = MutableLiveData<FirebaseUser?>()
    val currentUser = mutableCurrentUser as LiveData<FirebaseUser?>

    init {
        firebaseAuth.addAuthStateListener { auth ->
            mutableCurrentUser.value = auth.currentUser
        }
    }

    fun createUser(email: String, password: String) =
        firebaseAuth.createUserWithEmailAndPassword(email, password)

    fun updateUserProfile(profileUpdate: UserProfileChangeRequest): Task<Void> {
        val user = currentUser.value ?: throw RuntimeException("Trying to update user profile but user has not been authenticated")
        return user.updateProfile(profileUpdate)
    }

    fun signInWithEmailAndPassword(email: String, password: String) =
        firebaseAuth.signInWithEmailAndPassword(email, password)

}