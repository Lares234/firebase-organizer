package me.rodak.yezzir.model.firestore

import com.google.gson.annotations.SerializedName

data class TaskDto(
    @SerializedName("title") val title: String?,
    @SerializedName("notes") val notes: String?,
    @SerializedName("is_important") val isImportant: Boolean,
    @SerializedName("is_completed") val isCompleted: Boolean = false
)