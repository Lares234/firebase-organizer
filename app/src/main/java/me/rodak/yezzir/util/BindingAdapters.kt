package me.rodak.yezzir.util

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout
@BindingAdapter("animatedText")
fun TextView.setAnimatedText(newText: CharSequence?) {
    if (newText != null) {
        if (newText.isBlank()) {
            if  (visibility == View.VISIBLE) visibility = View.INVISIBLE
        } else {
            text = newText
            visibility = View.VISIBLE
            alpha = 0f
            animate().apply {
                alpha(1f)
                duration = 500L
            }.start()
        }
    } else {
        visibility = View.GONE
    }
}

@BindingAdapter("responsiveError")
fun TextInputLayout.setResponsiveError(errorText: CharSequence?) {
    errorText?.let {
        if (it.isEmpty()) {
            isErrorEnabled = false
        } else {
            isErrorEnabled = true
            error = errorText
        }
    }
}