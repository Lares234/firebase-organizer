package me.rodak.yezzir.util

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData

var <T> MutableLiveData<T>.postValue: T?
    @MainThread get() = value
    set(value) = postValue(value)