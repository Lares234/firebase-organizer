package me.rodak.yezzir.ui.base

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel

@SuppressWarnings("unused")
open class BaseAndroidViewModel(application: Application) : AndroidViewModel(application) {

    protected fun getString(@StringRes resId: Int) =
        getApplication<Application>().getString(resId)

    protected fun getString(@StringRes resId: Int, vararg formatArgs: Any) =
        getApplication<Application>().getString(resId, *formatArgs)
}