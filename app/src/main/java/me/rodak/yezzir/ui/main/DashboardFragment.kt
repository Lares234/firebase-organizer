package me.rodak.yezzir.ui.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.transition.MaterialFadeThrough
import me.rodak.yezzir.App
import me.rodak.yezzir.databinding.FragmentDashboardBinding
import me.rodak.yezzir.ui.base.BaseFragment
import me.rodak.yezzir.R
import me.rodak.yezzir.ui.NavigationViewModel

class DashboardFragment : BaseFragment<DashboardViewModel, FragmentDashboardBinding>() {
    override val layoutRes = R.layout.fragment_dashboard
    override fun daggerInject() = (requireActivity().application as App).component.inject(this)
    override fun getVmClass() = DashboardViewModel::class

    private val navController by lazy {
        findNavController()
    }
    private val activityViewModel by lazyActivityViewModels<NavigationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityViewModel.loadUserData()
        enterTransition = MaterialFadeThrough.create()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObservers()
    }

    private fun registerObservers() {
        viewModel.navigationAction.observe(viewLifecycleOwner, Observer { direction ->
            navController.navigate(direction)
        })
    }

}