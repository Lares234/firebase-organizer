package me.rodak.yezzir.ui

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import me.rodak.yezzir.R
import me.rodak.yezzir.ui.base.BaseActivity

class NavigationActivity : BaseActivity<NavigationViewModel>() {
    companion object {
        const val NAV_HOST_TAG = "nav_host"
    }

    override val layoutRes = R.layout.activity_navigation
    override fun daggerInject() = daggerComponent.inject(this)
    override fun getVmClass() = NavigationViewModel::class

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navHost = supportFragmentManager.findFragmentByTag(NAV_HOST_TAG) as NavHostFragment
        supportFragmentManager.beginTransaction().setPrimaryNavigationFragment(navHost).commit()

        navHost.navController.apply {
            graph = navInflater.inflate(R.navigation.main_nav_graph).apply {
                startDestination = if (viewModel.isUserAuthenticated) R.id.dashboard_fragment
                else R.id.login_main_fragment
            }
        }
    }
}
