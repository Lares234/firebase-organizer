package me.rodak.yezzir.ui.login

import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.ViewModel
import me.rodak.yezzir.R
import me.rodak.yezzir.util.SingleLiveEvent
import javax.inject.Inject

class LoginMainViewModel
@Inject constructor() : ViewModel() {
    val navigationAction = SingleLiveEvent<@IdRes Int>()

    fun onButtonClicked(view: View) {
        when (view.id) {
            R.id.sign_in_email_button -> navigationAction.setValue(R.id.action_login_main_to_login_email)
            R.id.sign_in_phone_number_button,
            R.id.sign_in_google_account_button -> {
                // TODO
            }
        }
    }
}