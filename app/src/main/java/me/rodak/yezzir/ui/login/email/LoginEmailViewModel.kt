package me.rodak.yezzir.ui.login.email

import android.app.Application
import android.util.Patterns
import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import kotlinx.coroutines.launch
import me.rodak.yezzir.R
import me.rodak.yezzir.model.FirebaseStorage
import me.rodak.yezzir.ui.base.BaseAndroidViewModel
import me.rodak.yezzir.util.SingleLiveEvent
import me.rodak.yezzir.util.await
import me.rodak.yezzir.util.postValue
import timber.log.Timber
import javax.inject.Inject

class LoginEmailViewModel @Inject constructor(
    application: Application,
    private val firebase: FirebaseStorage
) : BaseAndroidViewModel(application) {
    val navigationAction = SingleLiveEvent<@IdRes Int>()
    val serverResponseError = MutableLiveData<ServerResponseError>()
    val emailErrorText = MutableLiveData<CharSequence>()
    val passwordErrorText = MutableLiveData<CharSequence>()
    val authenticationErrorText = MutableLiveData<CharSequence>()
    val progressBarVisibility = MutableLiveData(View.GONE)
    val userInputEnabled = MutableLiveData(true)

    var userEmail = ""
    var userPassword = ""

    private fun startAuthentication() = viewModelScope.launch {
        authenticationErrorText.postValue = ""
        if (validateInput()) {
            try {
                updateUiForAuthPending(true)
                firebase.signInWithEmailAndPassword(userEmail, userPassword).await()
                navigationAction.postValue = R.id.action_login_email_to_dashboard
            } catch (e: FirebaseAuthInvalidCredentialsException) {
                serverResponseError.postValue = ServerResponseError.INVALID_CREDENTIALS
                authenticationErrorText.postValue = getString(R.string.invalid_password_error)
            } catch (e: FirebaseAuthInvalidUserException) {
                serverResponseError.postValue = ServerResponseError.INVALID_USER
                authenticationErrorText.postValue = getString(R.string.account_does_not_exist_error)
            } catch (e: FirebaseNetworkException) {
                authenticationErrorText.postValue = getString(R.string.server_connection_error)
            } catch (e: FirebaseException) {
                authenticationErrorText.postValue = getString(R.string.unknown_server_error).also { Timber.w(e) }
            } finally {
                updateUiForAuthPending(false)
            }
        }
    }

    private fun updateUiForAuthPending(isPending: Boolean) {
        if (isPending) {
            progressBarVisibility.postValue = View.VISIBLE
            userInputEnabled.postValue = false
        } else {
            progressBarVisibility.postValue = View.GONE
            userInputEnabled.postValue = true
        }
    }

    private fun validateInput(): Boolean {
        var valid = true

        if (userEmail.isBlank()) {
            emailErrorText.postValue = "This field cannot be empty"
            valid = false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            emailErrorText.postValue = "Please enter valid email address"
            valid = false
        } else {
            emailErrorText.postValue = ""
        }

        if (userPassword.isBlank()) {
            passwordErrorText.postValue = "This field cannot be empty"
            valid = false
        } else {
            passwordErrorText.postValue = ""
        }

        if (!valid) authenticationErrorText.postValue = null

        return valid
    }

    fun onButtonClick(view: View) {
        when (view.id) {
            R.id.sign_in_button -> startAuthentication()
            R.id.sign_up_button -> navigationAction.setValue(R.id.action_login_email_to_register_email)
            R.id.back_button -> navigationAction.setValue(R.id.action_login_email_pop_to_login_main)
        }
    }
}