package me.rodak.yezzir.ui.main

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import me.rodak.yezzir.R
import me.rodak.yezzir.model.FirebaseStorage
import me.rodak.yezzir.model.RoomStorage
import me.rodak.yezzir.util.SingleLiveEvent
import javax.inject.Inject

class DashboardViewModel @Inject constructor(
    private val firebase: FirebaseStorage,
    private val room: RoomStorage
) : ViewModel() {
    val navigationAction = SingleLiveEvent<NavDirections>()
    var currentUserName = Transformations.map(firebase.currentUser) {
        it?.displayName
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.create_task_button -> navigationAction.setValue(DashboardFragmentDirections.actionDashboardToCreateTaskDialog())
        }
    }

}