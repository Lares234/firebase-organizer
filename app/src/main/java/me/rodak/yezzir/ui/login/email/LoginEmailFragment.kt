package me.rodak.yezzir.ui.login.email

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.transition.MaterialFadeThrough
import com.google.android.material.transition.MaterialSharedAxis
import kotlinx.android.synthetic.main.fragment_login_email.*
import me.rodak.yezzir.App
import me.rodak.yezzir.R
import me.rodak.yezzir.databinding.FragmentLoginEmailBinding
import me.rodak.yezzir.ui.base.BaseFragment

class LoginEmailFragment : BaseFragment<LoginEmailViewModel, FragmentLoginEmailBinding>() {
    override val layoutRes = R.layout.fragment_login_email
    override fun daggerInject() = (activity?.application as App).component.inject(this)
    override fun getVmClass() = LoginEmailViewModel::class
    private val navController by lazy {
        findNavController()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
        reenterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
        exitTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObservers()
    }

    private fun registerObservers() {
        viewModel.serverResponseError.observe(viewLifecycleOwner, Observer { error ->
            error?.let {
                when (error) {
                    ServerResponseError.INVALID_CREDENTIALS,
                    ServerResponseError.INVALID_USER-> {
                        password_edit_text.text?.clear()
                    }
                }
            }
        })

        viewModel.navigationAction.observe(viewLifecycleOwner, Observer { actionId->
            when (actionId) {
                R.id.action_login_email_pop_to_login_main -> {
                    returnTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
                }
                R.id.action_login_email_to_dashboard -> {
                    returnTransition = MaterialFadeThrough.create()
                }
            }
            navController.navigate(actionId)
        })


    }
}