package me.rodak.yezzir.ui.login.email

import android.app.Application
import android.util.Patterns
import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.UserProfileChangeRequest
import kotlinx.coroutines.launch
import me.rodak.yezzir.R
import me.rodak.yezzir.model.FirebaseStorage
import me.rodak.yezzir.ui.base.BaseAndroidViewModel
import me.rodak.yezzir.util.SingleLiveEvent
import me.rodak.yezzir.util.await
import me.rodak.yezzir.util.postValue
import timber.log.Timber
import javax.inject.Inject

class RegisterEmailViewModel
@Inject constructor(application: Application, private val firebase: FirebaseStorage) : BaseAndroidViewModel(application) {
    companion object {
        const val PASSWORD_MIN_LENGTH = 6
    }

    internal val navigationAction = SingleLiveEvent<@IdRes Int>()
    val userNameErrorText = MutableLiveData<CharSequence>("")
    val userEmailErrorText = MutableLiveData<CharSequence>("")
    val userPasswordErrorText = MutableLiveData<CharSequence>("")
    val userPasswordRepeatErrorText = MutableLiveData<CharSequence>("")
    val serverResponseError = MutableLiveData<CharSequence>("")
    val progressBarVisibility = MutableLiveData(View.GONE)
    val userInputEnabled = MutableLiveData(true)

    var userName = ""
    var userEmail = ""
    var userPassword = ""
    var userRepeatPassword = ""

    fun onButtonClick(button: View) {
        when (button.id) {
            R.id.sign_up_button -> startAccountCreation()
            R.id.back_button -> navigationAction.setValue(R.id.action_register_email_pop_to_login_email)
        }
    }

    private fun startAccountCreation() = viewModelScope.launch {
        if (isInputValid()) {
            try {
                updateUiForAuthPending(true)
                firebase.createUser(userEmail, userPassword).continueWithTask { createTask ->
                    if (createTask.isSuccessful) requestUserProfileNameUpdate()
                    else Tasks.forException(createTask.exception!!)
                }.await()
                finishAccountCreation()
            } catch (e: FirebaseNetworkException) {
                serverResponseError.postValue = getString(R.string.server_connection_error)
            } catch (e: FirebaseAuthUserCollisionException) {
                userEmailErrorText.postValue = getString(R.string.email_collision_error)
            } catch (e: FirebaseException) {
                serverResponseError.postValue = getString(R.string.unknown_server_error).also { Timber.e(e) }
            } finally {
                updateUiForAuthPending(false)
            }
        }
    }

    private fun finishAccountCreation() {
        navigationAction.postValue = R.id.action_register_email_to_dashboard
    }

    private fun requestUserProfileNameUpdate(): Task<Void> = UserProfileChangeRequest.Builder().apply {
        setDisplayName(userName)
    }.build().let { profileUpdate ->
        firebase.updateUserProfile(profileUpdate)
    }

    private fun updateUiForAuthPending(isPending: Boolean) {
        if (isPending) {
            progressBarVisibility.postValue = View.VISIBLE
            userInputEnabled.postValue = false
        } else {
            progressBarVisibility.postValue = View.GONE
            userInputEnabled.postValue = true
        }
    }

    private fun isInputValid(): Boolean {
        var valid = true

        if (userName.isBlank()) {
            userNameErrorText.postValue = getString(R.string.field_cant_be_empty)
            valid = false
        } else {
            userNameErrorText.postValue = ""
        }

        if (userEmail.isBlank()) {
            userEmailErrorText.postValue = getString(R.string.field_cant_be_empty)
            valid = false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            userEmailErrorText.postValue = getString(R.string.email_invalid_form)
            valid = false
        } else {
            userEmailErrorText.postValue = ""
        }

        if (userPassword.isBlank()) {
            userPasswordErrorText.postValue = getString(R.string.field_cant_be_empty)
            userPasswordRepeatErrorText.postValue = " "
            valid = false
        } else if (userPassword.length < PASSWORD_MIN_LENGTH) {
            userPasswordErrorText.postValue = getString(R.string.password_too_short)
            userPasswordRepeatErrorText.postValue = " "
            valid = false
        } else if (userPassword != userRepeatPassword) {
            userPasswordErrorText.postValue = getString(R.string.password_mismatch)
            userPasswordRepeatErrorText.postValue = " "
            valid = false
        } else {
            userPasswordErrorText.postValue = ""
            userPasswordRepeatErrorText.postValue = ""
        }

        return valid
    }
}