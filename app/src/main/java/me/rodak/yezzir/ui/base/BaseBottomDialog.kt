package me.rodak.yezzir.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import me.rodak.yezzir.BR
import me.rodak.yezzir.dagger.DaggerViewModelFactory
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseBottomDialog<VM: ViewModel, B: ViewDataBinding> : BottomSheetDialogFragment() {
    protected abstract fun daggerInject()
    protected abstract fun getVmClass(): KClass<VM>

    @Inject
    protected lateinit var viewModelFactory: DaggerViewModelFactory
    private lateinit var dataBinding: B
    protected val viewModel: VM by lazyViewModels()
    protected abstract val layoutRes: Int
        @LayoutRes get

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let {
            daggerInject()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        dataBinding.setVariable(BR.viewModel, viewModel)
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }

    private fun lazyViewModels() =
        ViewModelLazy(getVmClass(), { viewModelStore }, { viewModelFactory })
}