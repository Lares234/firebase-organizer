package me.rodak.yezzir.ui.main.create

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.rodak.yezzir.R
import me.rodak.yezzir.model.RoomStorage
import me.rodak.yezzir.model.room.entity.TaskEntity
import me.rodak.yezzir.util.SingleLiveEvent
import javax.inject.Inject

class CreateTaskViewModel @Inject constructor(private val room: RoomStorage) : ViewModel() {
    val taskTitleText = MutableLiveData<String>()
    val taskNotesText = MutableLiveData<String>()
    private val taskGroup = MutableLiveData<Long>()
    val isTaskImportant = MutableLiveData(false)
    val isFinished = SingleLiveEvent<Boolean>()


    fun onChipClick(chip: View) {
        when (chip.id) {
            R.id.is_important_chip -> isTaskImportant.value = isTaskImportant.value?.not()
        }
    }

    fun onButtonClick(button: View) {
        when (button.id) {
            R.id.cancel_button -> isFinished.setValue(true)
            R.id.create_button -> createTask()
        }
    }

    private fun createTask() = viewModelScope.launch(Dispatchers.IO) {
        room.insertTask(
            TaskEntity(
                title = taskTitleText.value ?: "",
                notes = taskNotesText.value ?: "",
                groupId = taskGroup.value ?: 0,
                isImportant = isTaskImportant.value ?: false
            )
        )
        isFinished.postValue(true)
    }
}