package me.rodak.yezzir.ui.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.transition.MaterialSharedAxis
import me.rodak.yezzir.App
import me.rodak.yezzir.R
import me.rodak.yezzir.databinding.FragmentLoginMainBinding
import me.rodak.yezzir.ui.base.BaseFragment

class LoginMainFragment : BaseFragment<LoginMainViewModel, FragmentLoginMainBinding>() {
    override val layoutRes = R.layout.fragment_login_main
    override fun daggerInject() = (activity?.application as App).component.inject(this)
    override fun getVmClass() = LoginMainViewModel::class
    private val navController by lazy {
        findNavController()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
        reenterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
        exitTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
        returnTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObservers()
    }

    private fun registerObservers() {
        viewModel.navigationAction.observe(viewLifecycleOwner, Observer { actionId ->
            navController.navigate(actionId)
        })
    }
}