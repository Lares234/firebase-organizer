package me.rodak.yezzir.ui.main.create

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import me.rodak.yezzir.App
import me.rodak.yezzir.R
import me.rodak.yezzir.databinding.DialogCreateTaskBinding
import me.rodak.yezzir.ui.base.BaseBottomDialog

class CreateTaskBottomDialog : BaseBottomDialog<CreateTaskViewModel, DialogCreateTaskBinding>() {
    override val layoutRes = R.layout.dialog_create_task
    override fun daggerInject() = (activity?.application as App).component.inject(this)
    override fun getVmClass() = CreateTaskViewModel::class

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObservers()
    }

    private fun registerObservers() {
        viewModel.isFinished.observe(viewLifecycleOwner, Observer {
            dismiss()
        })
    }
}