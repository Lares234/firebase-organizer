package me.rodak.yezzir.ui.login.email

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.transition.MaterialFadeThrough
import com.google.android.material.transition.MaterialSharedAxis
import me.rodak.yezzir.App
import me.rodak.yezzir.databinding.FragmentRegisterEmailBinding
import me.rodak.yezzir.ui.base.BaseFragment
import me.rodak.yezzir.R

class RegisterEmailFragment : BaseFragment<RegisterEmailViewModel, FragmentRegisterEmailBinding>() {
    override val layoutRes = R.layout.fragment_register_email
    override fun daggerInject() = (activity?.application as App).component.inject(this)
    override fun getVmClass() = RegisterEmailViewModel::class
    private val navController by lazy {
        findNavController()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
        reenterTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
        exitTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObservers()
    }

    private fun registerObservers() {
        viewModel.navigationAction.observe(viewLifecycleOwner, Observer { actionId ->
            when (actionId) {
                R.id.action_register_email_pop_to_login_email -> {
                    returnTransition = MaterialSharedAxis.create(MaterialSharedAxis.X, false)
                }
                R.id.action_register_email_to_dashboard -> {
                    exitTransition = MaterialFadeThrough.create()
                }
            }
            navController.navigate(actionId)
        })
    }
}