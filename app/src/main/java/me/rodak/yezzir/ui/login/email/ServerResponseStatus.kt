package me.rodak.yezzir.ui.login.email

enum class ServerResponseError {
    INVALID_USER,
    INVALID_CREDENTIALS,
}