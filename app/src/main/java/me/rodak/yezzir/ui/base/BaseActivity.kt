package me.rodak.yezzir.ui.base

import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import me.rodak.yezzir.App
import me.rodak.yezzir.R
import me.rodak.yezzir.dagger.DaggerViewModelFactory
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseActivity<VM : ViewModel> : AppCompatActivity() {
    protected abstract fun daggerInject()
    protected abstract fun getVmClass(): KClass<VM>

    @Inject
    protected lateinit var viewModelFactory: DaggerViewModelFactory
    protected val viewModel by lazyViewModels()
    protected abstract val layoutRes: Int
        @LayoutRes get
    protected val daggerComponent by lazy {
        (application as App).component
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        daggerInject()
        setContentView(layoutRes)
    }

    private fun lazyViewModels() = ViewModelLazy(getVmClass(), { viewModelStore }, { viewModelFactory })
}