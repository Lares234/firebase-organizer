package me.rodak.yezzir.ui

import android.app.Application
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.rodak.yezzir.R
import me.rodak.yezzir.model.FirebaseStorage
import me.rodak.yezzir.model.RoomStorage
import me.rodak.yezzir.model.room.entity.GroupEntity
import me.rodak.yezzir.ui.base.BaseAndroidViewModel
import javax.inject.Inject

class NavigationViewModel
@Inject constructor(application: Application, private val firebase: FirebaseStorage, private val roomStorage: RoomStorage) :
    BaseAndroidViewModel(application) {
    private val groups
        get() = roomStorage.groups.value
    val isUserAuthenticated
        get() = firebase.currentUser.value != null

    fun loadUserData() = viewModelScope.launch {
        if (groups == null) {
            roomStorage.insertGroup(GroupEntity(name = getString(R.string.task_group_general)))
        }
    }
}