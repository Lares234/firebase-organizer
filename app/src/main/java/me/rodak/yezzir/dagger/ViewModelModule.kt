package me.rodak.yezzir.dagger

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.rodak.yezzir.ui.NavigationViewModel
import me.rodak.yezzir.ui.login.LoginMainViewModel
import me.rodak.yezzir.ui.login.email.LoginEmailViewModel
import me.rodak.yezzir.ui.login.email.RegisterEmailViewModel
import me.rodak.yezzir.ui.main.DashboardViewModel

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(NavigationViewModel::class)
    abstract fun bindNavigationViewModel(viewModel: NavigationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginEmailViewModel::class)
    abstract fun bindLoginEmailViewModel(viewModel: LoginEmailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterEmailViewModel::class)
    abstract fun bindRegisterEmailViewModel(viewModel: RegisterEmailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginMainViewModel::class)
    abstract fun bindLoginMainViewModel(viewModel: LoginMainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun bindDashboardViewModel(viewModel: DashboardViewModel): ViewModel
}