package me.rodak.yezzir.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Reusable
import java.lang.IllegalStateException
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Provider

@Reusable
class DaggerViewModelFactory
@Inject constructor(private val viewModelProviders: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) :
    ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val viewModel = viewModelProviders[modelClass]?.get()
            ?: throw IllegalStateException("Unknown model class $modelClass")

        try {
            return viewModel as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

}