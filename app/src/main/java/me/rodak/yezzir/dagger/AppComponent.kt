package me.rodak.yezzir.dagger

import dagger.Component
import me.rodak.yezzir.ui.login.LoginMainFragment
import me.rodak.yezzir.ui.login.email.LoginEmailFragment
import me.rodak.yezzir.ui.NavigationActivity
import me.rodak.yezzir.ui.login.email.RegisterEmailFragment
import me.rodak.yezzir.ui.main.DashboardFragment
import me.rodak.yezzir.ui.main.create.CreateTaskBottomDialog
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, StorageModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(injected: NavigationActivity)
    fun inject(injected: LoginMainFragment)
    fun inject(injected: LoginEmailFragment)
    fun inject(injected: RegisterEmailFragment)
    fun inject(injected: DashboardFragment)
    fun inject(injected: CreateTaskBottomDialog)
}