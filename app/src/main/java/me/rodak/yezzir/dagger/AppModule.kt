package me.rodak.yezzir.dagger

import android.app.Application
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import java.lang.RuntimeException

@Module
class AppModule(private val application: Application) {
    companion object {
        const val PREFERENCES_NAME = "yezzir_preferences"
    }

    @Provides
    fun provideApplication() = application

    @Provides
    fun provideFirebaseAuth() = FirebaseAuth.getInstance()

    @Provides
    fun provideFirestore() = FirebaseFirestore.getInstance()

    @Provides
    fun provideSharedPreferences() =
        application.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE) ?: throw RuntimeException("Failed to load SharedPreferences")
}