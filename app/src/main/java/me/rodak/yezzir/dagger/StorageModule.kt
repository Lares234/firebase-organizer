package me.rodak.yezzir.dagger

import android.app.Application
import android.content.SharedPreferences
import androidx.room.Room
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import me.rodak.yezzir.model.FirebaseStorage
import me.rodak.yezzir.model.PreferencesStorage
import me.rodak.yezzir.model.RoomStorage
import me.rodak.yezzir.model.room.Database
import me.rodak.yezzir.model.room.Database.Companion.DATABASE_NAME
import javax.inject.Singleton

@Module
class StorageModule {
    @Provides
    fun provideFirebaseStorage(auth: FirebaseAuth, firestore: FirebaseFirestore) = FirebaseStorage(auth, firestore)

    @Provides
    fun providePreferencesStorage(preferences: SharedPreferences) = PreferencesStorage(preferences)

    @Provides
    fun provideRoomStorage(database: Database) = RoomStorage(database)

    @Provides
    @Singleton
    fun provideDatabase(application: Application) = Room.databaseBuilder(application, Database::class.java, DATABASE_NAME).build()
}