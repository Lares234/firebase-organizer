package me.rodak.yezzir

import android.app.Application
import me.rodak.yezzir.dagger.AppComponent
import me.rodak.yezzir.dagger.AppModule
import me.rodak.yezzir.dagger.DaggerAppComponent
import timber.log.Timber

class App : Application() {
    val component: AppComponent by lazy {
        DaggerAppComponent.builder().apply {
            appModule(AppModule(this@App))
        }.build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}